package com.example.devpc.projectpicture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ljubomir on 9.12.2014..
 */
public class EditProjectActivity extends ActionBarActivity {

    private static final int CAMERA_REQUEST = 1888;

    private Button bNewPicture;
    private Button bDeletePicture;
    private Button bEditPicture;
    private Button bShowPicture;
    private Button bBack;

    private TextView tvTitle;

    private RadioGroup rgEditProject;

    private String[] pictureList;
    private String directoryName;
    private int selectedPicture = -1;

    private Bitmap image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);
        directoryName = SaveSharedPreference.getEditProjectName(EditProjectActivity.this);

        tvTitle = (TextView)findViewById(R.id.tv_edit_project_title);
        tvTitle.setText("Project: " + directoryName);

        bNewPicture = (Button) findViewById(R.id.b_edit_data_project_new_picture);
        bNewPicture.setOnClickListener(newPictureOCL);

        bDeletePicture = (Button) findViewById(R.id.b_edit_project_delete_picture);
        bDeletePicture.setOnClickListener(deletePictureOCL);

        bEditPicture = (Button) findViewById(R.id.b_edit_project_edit_picture);
        bEditPicture.setOnClickListener(editPictureOCL);

        rgEditProject = (RadioGroup) findViewById(R.id.rg_edit_project);
        rgEditProject.setOnCheckedChangeListener(rgEditProjectOCCL);
        rwdData(1, "");
        if(pictureList != null){

            addRadioButtons(pictureList.length);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // OCL is OnClickListener
    //Back OnClickListener
    private View.OnClickListener backOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            finish();
        }
    };

    //Back OnClickListener
    private View.OnClickListener deletePictureOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectedPicture > -1){

                rwdData(2, pictureList[selectedPicture]);
                rwdData(1, "");
                addRadioButtons(pictureList.length);
                selectedPicture = -1;
            }
            else{

                Toast.makeText(EditProjectActivity.this, R.string.warning_for_delete, Toast.LENGTH_LONG).show();
            }
        }
    };

    //New_picture OnClickListener
    private View.OnClickListener newPictureOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //Intent intent = new Intent(EditProjectActivity.this, NewPictureActivity.class);
            //startActivity(intent);

            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    };

    //Edit_picture OnClickListener
    private View.OnClickListener editPictureOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectedPicture > -1){

                SaveSharedPreference.setEditPictureName(EditProjectActivity.this, pictureList[selectedPicture]);
                Intent intent = new Intent(EditProjectActivity.this, EditPicture2Activity.class);
                startActivity(intent);
            }
            else {

                Toast.makeText(EditProjectActivity.this, R.string.warning_for_edit, Toast.LENGTH_LONG).show();
            }
        }
    };

    // OCCL is OnCheckedChangeListener
    private RadioGroup.OnCheckedChangeListener rgEditProjectOCCL = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            selectedPicture = checkedId;
        }
    };

    //RadioGroup set radio of project picture
    private void addRadioButtons(int numButtons) {
        rgEditProject.removeAllViews();
        for(int i = 0; i < numButtons; i++){
            //instantiate...
            RadioButton radioButton = new RadioButton(this);

            //set the values that you would otherwise hardcode in the xml...
            radioButton.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT));

            //label the button...
            radioButton.setText(pictureList[i]);
            radioButton.setId(i);

            //add it to the group.
            rgEditProject.addView(radioButton, i);
        }
    }

    //Read, Write and Write data to memory path = data/data/package_name/files + project directory name
    private boolean rwdData(int type, String picture_name){
        if(type == 0){
            //write
            File file = new File(getFilesDir() + "/"+ directoryName + "/" + picture_name);
            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(file);

                // Use the compress method on the BitMap object to write image to the OutputStream
                image.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        /*if(type == 0){
            //write
            //File file = new File(getFilesDir(), "/" + directory_name + "/" + picture_name);
            //file.mkdirs();
            //write
            BufferedWriter bufferedWriter = null;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(new File(getFilesDir() + File.separator + directory_name + File.separator + "Picture.jpg")));
                bufferedWriter
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
        else if(type == 1){
            //read
            File file = new File(getFilesDir(), "/" + directoryName);
            if(file.isDirectory() == false)
            {
                Toast.makeText(this, "ERROR 1", Toast.LENGTH_SHORT).show();
                return false;
            }
            File[] files = file.listFiles();
            if(file.length()>0){

                pictureList = new String[files.length];

            }
            int i = 0;
            for(File f : files)
            {
                if(f.isDirectory() || f.isFile())
                {
                    try
                    {
                        pictureList[i++] = f.getName();
                    }
                    catch(Exception e){

                        return false;
                    }
                }
            }
        }
        else if(type == 2){
            //delete
            File file = new File(getFilesDir(), "/" + directoryName);
            File[] files = file.listFiles();
            int i = 0;
            for(File f : files)
            {
                if(f.isDirectory() || f != null)
                {
                    try
                    {
                        if(f.getName().compareTo(picture_name) == 0){

                            f.delete();
                        }
                    }
                    catch(Exception e){

                        return false;
                    }
                }
            }
        }
        return true;
    }

    //Save image on internal memory after capture and set new radio button
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String currentDateandTime = sdf.format(new Date());
            image = (Bitmap) data.getExtras().get("data");
            rwdData(0, currentDateandTime.toString() + ".jpg");
            rwdData(1, "");
            if(pictureList != null){

                addRadioButtons(pictureList.length);
            }
            image = null;
        }
    }
}
