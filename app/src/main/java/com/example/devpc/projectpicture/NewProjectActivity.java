package com.example.devpc.projectpicture;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

/**
 * Created by Ljubomir on 9.12.2014..
 */
public class NewProjectActivity extends ActionBarActivity {

    private Button bCreate;
    private Button bBack;
    private EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);

        bCreate = (Button) findViewById(R.id.b_new_project_create);
        bCreate.setOnClickListener(newProjectCreateOCL);

        bBack = (Button) findViewById(R.id.b_new_project_back);
        bBack.setOnClickListener(newProjectBackOCL);

        etName = (EditText) findViewById(R.id.et_new_project_name);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // OCL is OnClickListener
    //Listener on Create new project
    private View.OnClickListener newProjectCreateOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(etName.getText().toString().length()>0){

                if(writeDirectory(etName.getText().toString())) {

                    Intent intent = new Intent(NewProjectActivity.this, MainActivity.class);
                    startActivity(intent);

                }

            }
        }
    };

    // Make a directory for create
    private boolean writeDirectory(String directory){

        //write
        File file = new File(getFilesDir(), "/" + directory);
        if(file.mkdirs() == false){

            Toast.makeText(NewProjectActivity.this, "Duplicate!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    // Back button
    private View.OnClickListener newProjectBackOCL = new View.OnClickListener(){

        @Override
        public void onClick(View v) {

            finish();
        }
    };
}
