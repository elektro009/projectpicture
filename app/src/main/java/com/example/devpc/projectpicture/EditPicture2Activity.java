package com.example.devpc.projectpicture;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ZoomControls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by Ljubomir on 2.1.2015..
 */
public class EditPicture2Activity extends ActionBarActivity {

    TextView tvTitle;
    ImageView ivEditPicture2;
    Button bNewPoint;
    Button bDeletePoint;
    Button bFinish;
    ZoomControls zcEditPicture2;

    float x, y = 0;
    String picturePath = null;
    Bitmap image;
    int[][] matrix = new int[][]{   {1,1,1,1,1,0},
                                    {1,1,1,0,0,0},
                                    {1,1,1,1,0,0},
                                    {1,0,1,1,1,0},
                                    {1,0,0,1,1,1},
                                    {0,0,0,0,1,1}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_picture2);

        //Title
        tvTitle = (TextView) findViewById(R.id.tv_edit_picture2_title);
        tvTitle.setText(SaveSharedPreference.getEditPictureName(EditPicture2Activity.this));

        //set picture_path
        picturePath = SaveSharedPreference.getEditProjectName(EditPicture2Activity.this) + "/" + SaveSharedPreference.getEditPictureName(EditPicture2Activity.this);

        //read image
        wrData(1);

        ivEditPicture2 = (ImageView) findViewById(R.id.iv_edit_picture2);
        ivEditPicture2.setImageBitmap(image);
        ivEditPicture2.setScaleType(ImageView.ScaleType.FIT_CENTER); // make the image fit to the center.
        //Button
        bFinish = (Button)findViewById(R.id.b_edit_picture2_finish);
        bNewPoint = (Button)findViewById(R.id.b_edit_picture2_new_point);
        bDeletePoint = (Button)findViewById(R.id.b_edit_picture2_delete_point);
        zcEditPicture2 = (ZoomControls)findViewById(R.id.zoomControls);

        //Button setListener
        bFinish.setOnClickListener(bEditPicture2FinishOCL);
        bNewPoint.setOnClickListener(bEditPicture2NewPointOCL);
        zcEditPicture2.setOnZoomInClickListener(zcEditPicture2OnZoomInOCL);
        zcEditPicture2.setOnZoomOutClickListener(zcEditPicture2OnZoomOutOCL);
        ivEditPicture2.setOnTouchListener(ivEditPicture2OCL);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // OCL is OnClickListener
    //Listener for edit_picture_finish
    private View.OnClickListener bEditPicture2FinishOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //save picture
            finish();
        }
    };

    //Listener for edit_picture_new_point
    private View.OnClickListener bEditPicture2NewPointOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //setPointOnImage();
            ivEditPicture2.setImageBitmap(image);
        }
    };

    //Zoom in Listener
    private View.OnClickListener zcEditPicture2OnZoomInOCL= new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            float x = ivEditPicture2.getScaleX();
            float y = ivEditPicture2.getScaleY();
            float  zomScale = 5;

            if(x < EditPicture2Activity.this.x + zomScale && y < EditPicture2Activity.this.y + zomScale){

                ivEditPicture2.setScaleX(x + 1);
                ivEditPicture2.setScaleY(y + 1);
            }
        }
    };

    //Zoom out Listener
    private View.OnClickListener zcEditPicture2OnZoomOutOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            float x = ivEditPicture2.getScaleX();
            float y = ivEditPicture2.getScaleY();
            if(EditPicture2Activity.this.x < x && EditPicture2Activity.this.y < y){

                ivEditPicture2.setScaleX(x - 1);
                ivEditPicture2.setScaleY(y - 1);
            }

        }
    };

    //ImageView on touch listener
    private View.OnTouchListener ivEditPicture2OCL= new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction() == MotionEvent.ACTION_DOWN)
            {
                int px, py;
                float heightIView, widthIView, widthImage, heightImage, ratio;
                heightIView = (float)ivEditPicture2.getHeight();
                widthIView = (float)ivEditPicture2.getWidth();
                heightImage = (float)image.getHeight();
                widthImage = (float)image.getWidth();
                ratio = (float)image.getWidth()/(float)ivEditPicture2.getWidth();
                px = (int)(event.getX() * ratio);
                py = (int)((event.getY() - (heightIView - widthIView * heightImage / widthImage) / 2) * ratio);
                //py = Iv_edit_picture2.getHeight();
                //px = Iv_edit_picture2.getWidth();
                setPointOnImage(px, py);
                ivEditPicture2.setImageBitmap(image);
                tvTitle.setText(px + ", " + py);
                return true;
            }
            return false;
        }
    };

    // Read Write image to internal memory
    private void wrData(int type){

        if(type == 0){

        }
        else if(type == 1){
            try {
                File f = new File(getFilesDir() + "/" + picturePath);
                image = BitmapFactory.decodeStream(new FileInputStream(f));
                image = image.copy(image.getConfig(), true);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
        }
    }

    void setPointOnImage(int x, int y){

        int k = 0, l = 0;
        int color = (0xFF << 24) | (255 << 16) | (10 << 8) | 10;

        for(int i = x; i < x + matrix.length; i++){
            for(int j = y; j < y + matrix.length; j++){
                if(matrix[k][l++] == 1 && i > -1 && i < image.getWidth() && j > -1 && j < image.getHeight()) {
                    image.setPixel(i, j, color);
                }
            }
            k++;
            l = 0;
        }
    }
}
