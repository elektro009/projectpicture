package com.example.devpc.projectpicture;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Ljubomir on 11.12.2014..
 */
public class SaveSharedPreference {

    private static final String EDIT_PROJECT_NAME = "project_name";
    private static final String EDIT_PICTURE_NAME = "picture_name";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setEditProjectName(Context ctx, String edit_project_name)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(EDIT_PROJECT_NAME, edit_project_name);
        editor.commit();
    }
    public static void setEditPictureName(Context ctx, String edit_picture_name)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(EDIT_PICTURE_NAME, edit_picture_name);
        editor.commit();
    }

    public static String getEditProjectName(Context ctx){

        return getSharedPreferences(ctx).getString(EDIT_PROJECT_NAME, "");
    }
    public static String getEditPictureName(Context ctx){

        return getSharedPreferences(ctx).getString(EDIT_PICTURE_NAME, "");
    }

    public static void clearData(Context ctx)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear(); //clear all stored data
        editor.commit();
    }


}
