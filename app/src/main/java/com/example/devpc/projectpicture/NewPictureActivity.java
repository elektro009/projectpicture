package com.example.devpc.projectpicture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by Ljubomir on 11.12.2014..
 */
public class NewPictureActivity extends ActionBarActivity {

    private static final int CAMERA_REQUEST = 1888;
    private ImageView ivImage;
    private Button bTryNew;
    private Button bBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_picture);
        ivImage = (ImageView) findViewById(R.id.iv_new_picture_cam);
        bTryNew = (Button) findViewById(R.id.b_new_picture_try_new);
        bTryNew.setOnClickListener(newPictureTryNewOCL);
        bBack = (Button) findViewById(R.id.b_new_picture_back);
        bBack.setOnClickListener(newPictureBackOCL);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // OCL is OnClickListener
    // Try New picture OnClickListener
    private View.OnClickListener newPictureTryNewOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    };

    private View.OnClickListener newPictureBackOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            finish();
        }
    };

    //Set image on ImageView when capture image.
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode != 0) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            ivImage.setImageBitmap(photo);
        }
    }
}
