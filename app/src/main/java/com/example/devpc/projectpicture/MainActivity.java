package com.example.devpc.projectpicture;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class MainActivity extends ActionBarActivity {

    private Button newProject;
    private Button sendProject;
    private Button editProject;
    private Button deleteProject;

    private RadioGroup rgProject;

    private String[] directory;
    private int selectedDirectory = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set Listener
        newProject = (Button) findViewById(R.id.b_new_project);
        newProject.setOnClickListener(newProjectOCL);

        sendProject = (Button) findViewById(R.id.b_send_project);
        sendProject.setOnClickListener(sendProjectOCL);

        editProject = (Button) findViewById(R.id.b_edit_project);
        editProject.setOnClickListener(editProjectOCL);

        deleteProject = (Button) findViewById(R.id.b_delete_project);
        deleteProject.setOnClickListener(deleteProjectOCL);

        rgProject = (RadioGroup) findViewById(R.id.rg_project);
        rgProject.setOnCheckedChangeListener(rgProjectSetOCCL);
        rwDirectory(1, "");
        if(directory != null){

            addRadioButtons(directory.length);

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //OCL is OnClickListener
    //New project button listener
    private View.OnClickListener newProjectOCL = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, NewProjectActivity.class);
            startActivity(intent);
        }
    };

    //Send project button listener
    private View.OnClickListener sendProjectOCL = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            //Intent intent = new Intent(MainActivity.this, Send_projectActivity.class);
            //startActivity(intent);
        }
    };

    //Edit project button listener
    private View.OnClickListener editProjectOCL = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            if(selectedDirectory > -1){

                SaveSharedPreference.setEditProjectName(MainActivity.this, directory[selectedDirectory]);
                Intent intent = new Intent(MainActivity.this, Edit_projectActivity.class);
                startActivity(intent);
            }
            else{
                Toast.makeText(MainActivity.this, R.string.warning_for_edit, Toast.LENGTH_LONG).show();
            }
        }
    };

    //Delete project button listener
    private View.OnClickListener deleteProjectOCL = new View.OnClickListener(){

        @Override
        public void onClick(View v) {

            if(selectedDirectory > -1){

                int length = 0;
                rwDirectory(2, directory[selectedDirectory]);
                rwDirectory(1, "");
                if(directory != null){
                    length = directory.length;
                }
                addRadioButtons(length);
                selectedDirectory = -1;

            }
            else{
                Toast.makeText(MainActivity.this, R.string.warning_for_delete, Toast.LENGTH_LONG).show();
            }
        }
    };

    //OCCL is OnCheckedChangeListener
    //Radio Group setOnCheckedChangeListener
    private RadioGroup.OnCheckedChangeListener rgProjectSetOCCL = new RadioGroup.OnCheckedChangeListener(){

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            selectedDirectory = checkedId;
        }
    };

    //Read and Write data to memory path = data/data/package_name/files
    private boolean rwDirectory(int type, String directory_name){
        if(type == 0){
            //write
            File file = new File(getFilesDir(), "/"+ directory_name);
            if(file.mkdirs() == false){
            }
        }
        else if(type == 1){
            //read
            File file = new File(getFilesDir(),"");
            if(file.isDirectory() == false)
            {
                Toast.makeText(this, "ERROR 1", Toast.LENGTH_SHORT).show();
                return false;
            }
            File[] files = file.listFiles();
            if(file.length()>0){

                directory = new String[files.length];

            }
            int i = 0;
            for(File f : files)
            {
                if(f.isDirectory())
                {
                    try
                    {
                        directory[i++] = f.getName();
                    }
                    catch(Exception e){

                        return false;
                    }
                }
            }
        }
        else if(type == 2){
            //read
            File file = new File(getFilesDir(), directory_name);
            deleteRecursive(file);
            /*
            File[] files = file.listFiles();
            int i = 0;
            for(File f : files)
            {
                if(f.isDirectory() || f != null)
                {
                    try
                    {
                        if(f.getName().compareTo(directory_name) == 0){

                            f.delete();
                        }
                    }
                    catch(Exception e){

                        return false;
                    }
                }
            }*/
        }
        return true;
    }

    private void deleteRecursive(File fileOrDirectory){

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    //Read and Write data to memory path = data/data/package_name/files
    private String readWrite(int type){
        if(type == 0){
            //write
            BufferedWriter bufferedWriter = null;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(new File(getFilesDir()+File.separator+"MyFile.txt")));
                bufferedWriter.write("ljubomir");
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(type == 1){
            //read
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new FileReader(new File(getFilesDir()+File.separator+"MyFile.txt")));
                String read;
                StringBuilder builder = new StringBuilder("");

                while((read = bufferedReader.readLine()) != null){
                    builder.append(read);
                }
                Log.d("Output", builder.toString());
                bufferedReader.close();
                return builder.toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    //RadioGroup set radio of project
    private void addRadioButtons(int numButtons) {
        rgProject.removeAllViews();
        for(int i = 0; i < numButtons; i++){
            //instantiate...
            RadioButton radioButton = new RadioButton(this);

            //set the values that you would otherwise hardcode in the xml...
            radioButton.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT));

            //label the button...
            radioButton.setText(directory[i]);
            radioButton.setId(i);

            //add it to the group.
            rgProject.addView(radioButton, i);
        }
    }

}
